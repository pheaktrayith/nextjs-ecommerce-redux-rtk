import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Providers } from "./providers";
import Navbar from "@/components/Navbar";
import { roboto, suwannaphum } from "./font";
import Footer from "@/components/Footer";
import StoreProvider from "./storeProvider";
import SessionWrapper from "./SessionWrapper";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


const inter = Inter({ subsets: ["latin"] });


export const metadata: Metadata = {
	title: "KhStore ecommerce website",
	description:
		"khmer ecommerce website for shopping online in cambodia with free delivery and cash on  payment method for all products and services in cambodia.",
	openGraph: {
		title: "KhStore ecommerce website",
		description:
			"khmer ecommerce website for shopping online in cambodia with free delivery and cash on  payment method for all products and services in cambodia.",
		type: "website",
		locale: "en_US",
		url: "https://khstore.sen-quiz.tech",
		emails: "yithsopheaktra18@gmail.com",
		phoneNumbers: "+855 96 717 4832",
		siteName: "KhStore",
		countryName: "Cambodia",
		images: "https://store.istad.co/media/icon_images/ecommerce.png",
	},
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="en">
			<SessionWrapper>
				<body className={`${suwannaphum.variable} ${roboto.variable}`}>
					<StoreProvider>
						<Providers>
							<Navbar />
							{children}
							<ToastContainer />
							<Footer />
						</Providers>
					</StoreProvider>
				</body>
			</SessionWrapper>
		</html>
	);
}
